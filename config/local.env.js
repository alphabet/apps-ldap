'use strict';


module.exports = {
 admlogin: "reader",
 admkey: "MySecret",
 mongo: {
    uri: process.env.MONGODB_URI
         || 'mongodb://localhost:27017/meteor',
    options: {
             useMongoClient: true
             }
  },
port : process.env.PORT || 1389

};
