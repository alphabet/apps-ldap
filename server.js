'use strict';
// Set default node environment to production
process.env.NODE_ENV = process.env.NODE_ENV || 'production';
var mongoose = require('mongoose');
const config = require('./config/local.env');
var ldap = require('ldapjs');
var parseFilter = require('ldapjs').parseFilter;
const admin=config.admlogin;
const password=config.admkey;
// Connect to database
mongoose.connect(config.mongo.uri, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
});

mongoose.connection.on('error', function(err) {
  console.error(`MongoDB connection error: ${err}`);
  process.exit(-1);
});

// Pseudo User model
const User = mongoose.model('User', { });

var query=User.find({isActive: true})  //  Read  users
var SUFFIX='dc=Apps';
var ldapUsers = {};
var r = {}

function authorize(req, res, next) {
 if (req.connection.ldap.bindDN.toString() =="" || req.connection.ldap.bindDN.equals('cn=anonymous') )
    return next(new ldap.InsufficientAccessRightsError());
  return next();
}

function toLdap(u) {  // Convert to ldap format
r={
dn: 'cn='+u.username.toLowerCase()+','+SUFFIX,
attributes: {
cn: u.username.toLowerCase(),
username:  u.username,
uid: u.username,
uuid: u.username.toLowerCase(),
name: u.lastName ,
surname: u.firstName,
structure: u.structure,
mail: (!u.emails)?"":u.emails[0].address ,
nclocator: (!u.nclocator?"":u.nclocator),
//isActive: u.isActive,
profile: u.profile,
objectclass: 'appsUser',
//createtimestamp: Date.now() - 10,
}
};
return(r);
}

var server = ldap.createServer();

server.listen(config.port, function() {
console.log("Ldap Server Started %s", server.url)
});

// LDAPBIND
//Pseudo Bind via config

server.bind(SUFFIX, function(req, res, next) { 

var dn=req.dn.toString();
var uid=dn.split(",")[0].split("=")[1];
var key = req.credentials;

if ((uid != admin) || (key != password))
{
  console.log("BIND  %s %s Not Ok ", uid,key );

  return next(new ldap.InvalidCredentialsError());
} 
res.end();
return next();
});



// LDAPSEARCH

server.search(SUFFIX, authorize,   function(req, res, next) {
var dn = req.dn.toString();
var filter=parseFilter(req.filter.toString());
User.
  find({isActive: true }).
  cursor().
  on('data', function(doc) {
   // console.log(doc._doc); 
    var user= toLdap(doc._doc);

  if (filter.matches(user.attributes))
        {
         // console.log(user)
          res.send(user);
        }
          
}).
  on('end', function() { res.end(); return next() });
})
