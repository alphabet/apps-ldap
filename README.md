**Pseudo LDAP SERVER**

Read users  from database (MongoDB of laboite) and  respond to ldapsearch requests

Usage :
```
cp config/local.env-sample.js config/local.env.js
edit config/local.env.js
npm install
node server.js
```
Search :
ldapsearch -x -H ldap://localhost:1389 -D "cn=reader,dc=Apps" -w MySecret -b dc=Apps
